#!/usr/bin/env python

"""
assembly.py - Phenny Assembly Module
Author: devel@lorea.org
About: https://lorea.org
"""
import popen2

class Megahal(object):
  def __init__(self):
    self.input, self.output = popen2.popen2("megahal -pwb -d /home/p2501/lorea")
  
  def readSentence(self):
    return self.input.readline()
  
  def writeSentence(self, text):
    #print "yeye" + text
    self.output.write(text + "\n\n")
    self.output.flush()
    self.output.write('#SAVE' + "\n\n")
    self.output.flush()

hal = False

def mega(phenny,input):
   """Function that handle a conversation with megahal"""
   global hal
 
   if not hal:
      hal = Megahal()
      phenny.say(hal.readSentence()) 

   hal.writeSentence(str(input).split(':')[1])
   phenny.say(hal.readSentence())
mega.rule = r'$nickname:*\b'
mega.priority = 'high'


if __name__ == '__main__': 
   print __doc__.strip()
