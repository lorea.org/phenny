#!/usr/bin/env python
"""
assembly.py - Phenny Assembly Module
Author: devel@lorea.org
About: https://lorea.org
"""

class AssemblyBot(object):
  def __init__(self):
    self.assembly = False
    self.blah = 1
    self.lista = []
    self.atention = {} 

private = False

def get_private(phenny):
  global private
  if not private:
     private = AssemblyBot()
  #if not hasattr(phenny, 'assembly'):
  #phenny.assembly = AssemblyBot()
  #return phenny.assembly
  return private

#def set_private(data):
#  global assembly
#  assembly = data

#def modeOn(phenny, input): 
#  priv = get_private(phenny)
#  print priv
#  if input.admin:
#     if not priv.assembly:
#        phenny.say('Assembly mode on')
#  	priv.assembly = True
#        #set_private(True)
#        phenny.write(('MODE',input.sender,'+m'))
#        print 'Assembly mode on'
#     else:
#        phenny.say("Assembly was active")	
#modeOn.rule = r'(assembly)\s(on)\b'

#def modeOff(phenny, input):
#   priv = get_private(phenny)
#   print priv
#   if input.admin:
#      if priv.assembly:
#         phenny.say("Assembly mode off")
#	 phenny.write(('MODE',input.sender,'-m'))
#         priv.assembly = False
#         print 'Assembly mode off'
#      else:
#         phenny.say("Assembly mode was not activated")
#modeOff.rule = r'(assembly)\s(off)\b'

def oper(phenny, input):
  #input.sender puede ser el canal si se le habla desde uno o el nick del
  #que le habla
  priv = get_private(phenny)
  #phenny.write(('MODE',input.sender, '+o'), input.nick)
  if input.nick not in priv.lista:
     #phenny.say(input.nick + ": enter in the queue")
     priv.lista.append(input.nick)
     #phenny.say(str(priv.lista))
  #else:
     #phenny.say(input.nick + ": you are in the queue") 
oper.rule = r'(\+q)\b'
oper.priority = 'high'

def deoper(phenny, input):
  priv = get_private(phenny)
  #phenny.write(('MODE',input.sender,'-o'),input.nick)
  if input.nick in priv.lista:
     #phenny.say(input.nick + ": left the queue")
     priv.lista.remove(input.nick)
     phenny.say(str(priv.lista))
  #else:
     #phenny.say(input.nick + ": you are not in the queue")
deoper.rule = r'(\-q)\b'
deoper.priority = 'high'

def who(phenny, input):
   priv = get_private(phenny)
   phenny.say(str(priv.lista))
who.rule =r'(queue|turno)\b'
who.priority = 'high'

#Metodo que te da derecho a replica

#Si no habla durante ciertos mensajes que diga si no tienes nada que decir
#Lee todos los usuarios conectados al canal si hablas mas de 4 veces te quita de la lista
#cada vez que quite a dos increpa a los demas
#Meterlo en un diccionario mejor que una lista asi se mete campo y numero de veces que ha hablado
#def atencion(phenny, input):
#   priv = get_private(phenny)
#   phenny.say(str(priv.atention))
#   if input.nick not in priv.atention:
#      priv.atention.update({input.nick:1})
#      phenny.say(str(priv.atention))
#   else:
#      priv.atention[input.nick]=priv.atention[input.nick]+1
#      phenny.say(str(priv.atention))
#   val = motivation(phenny, priv, input)
#   #if motivation(phenny, priv, input) > 2:
#   phenny.say("UIUIi " + str(val))
#atencion.rule = r'.*'
#atencion.priority = 'high'

#def motivation(phenny, priv, input):
#   i = 0
#   print "dentro"
#   phenny.write(('NAMES',input.sender))
#   phenny.say('Hello friends ' + phenny.data)
#   for n, a in priv.atention.iteritems():
#      #Si se ha escrito mas de 4 veces lo quitamos del diccionario
#      if a > 4:
#         print 'hola' + n 
#         priv.atention.pop(n)
#         phenny.say("BORRADO: " + n)
#         i=i+1
#   return i

if __name__ == '__main__': 
   print __doc__.strip()
